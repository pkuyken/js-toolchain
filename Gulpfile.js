const gulp = require('gulp');
const babel = require('gulp-babel');
const gutil = require('gulp-util');
const Server = require('karma').Server;

gulp.task('default', () => {
    gutil.log('[default] - Executing default task');
    return gulp.src('src/**/*.js')
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(gulp.dest("dist"));

});

gulp.task('test', (done) => {
    new Server({
        configFile: __dirname + '/karma.conf.js',
        singleRun: true
    }, done).start();
});
